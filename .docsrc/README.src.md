# Stories
Backend software for writing journalistic stories & easily sharing your stories.

This is brand new, beta software & the documentation is poor & implementation details will change.

## Install
@template(php/composer_install, taeluf/stories)

## Getting Started
You make a `Stories` directory, then in that make a dir for each story you write. Then for each story, you'll need to instantiate a story object ... for now.

Sample deliver script
```php
@file(test/Server/deliver.php)
```

See the rest of the story files in @see_file(test/Server/Stories/sample1/)

`Stories/sample1/story.php`:
```php
@file(test/Server/Stories/sample1/story.php)
```

`Stories/sample1/story/main.md`:
```php
@file(test/Server/Stories/sample1/story/main.md)
```

