<?php

namespace Tlf;

class Story {

    protected string $dir;
    protected string $prefix;
    protected $lia;

    /**
     * an instance of Liaison CommonMark addon
     */
    public object $cmark;

    public function __construct($dir, $lia, $prefix){
        $this->dir = $dir;
        $this->prefix = $prefix;
        $this->lia = $lia;
    }

    public function setup(){
        $lia = $this->lia;
        $prefix = $this->prefix;
        $lia->addRoute($prefix , [$this, 'deliver']);

        $lia->addRoute("${prefix}files/", [$this, 'show_files']);
        // $site->setup_public_routes($root_dir.'/cov-pub/', '/covid/');

        $router = $lia->addon('lia:server.router');
        $patterns = $router->dir_to_patterns($this->dir.'/files/');

        foreach ($patterns as $file=>$pattern){
            $route = $prefix.'files'.$pattern;
            if (substr($route,-1)=='/')$route = substr($route,0,-1);
            $router->addRoute($route, $this->dir.'/files/'.$file);
        }
    }

    public function route_substories($name){
        $lia = $this->lia;
        $prefix = $this->prefix;
        $router = $lia->addon('lia:server.router');
        $patterns = $router->dir_to_patterns($this->dir.'/'.$name.'/');

        // print_r($patterns);
        // exit;
        foreach ($patterns as $file=>$pattern){
            $route = $prefix.$name.$pattern.'/';
            if (substr($route,-1)=='/')$route = substr($route,0,-1);
            $file=$this->dir.'/'.$name.'/'.$pattern;
            // $url = str_replace('//', '/', $route);
            $router->addRoute($route, [$this,'deliver_substory']);
            // echo $route;
            // exit;
            // var_dump($url);
            // exit;
        }
    }

    /**
     * echo the content of a file in your story directory
     */
    public function story($name){
        require($this->dir.'/story/'.$name.'.md');
    }

    public function file($name){
        return $this->prefix.'files/'.$name;
    }
    /**
     * show a list of links to sub-stories found at root_story_dir/$name
     */
    public function sub_stories($name){
        $files = \Lia\Utility\Files::all($this->dir.'/'.$name,$this->dir.'/'.$name);
        $router = $this->lia->addon('lia:server.router');
        $prefix = $this->prefix;
        echo '<ul>';
        foreach ($files as $f){
            $pattern = $router->fileToPattern($name.'/'.$f);
            $clean_pattern = $pattern;
            $pattern = $prefix.$pattern;
            $pattern=$this->lia->clean_url($pattern);
            echo '<li><a href="'.$pattern.'">'.$clean_pattern.'</a></li>';
        }
        echo '</ul>';
    }

    public function deliver($route, $response){
        $this->cmark->enable_extension('table_of_contents');
        $this->cmark->enable_extension('footnote');
        ob_start();
        require($this->dir.'/story.php');
        // echo file_get_contents($this->dir.'/sources.md');
        $response->content = ob_get_clean();
    }

    public function deliver_substory($route, $response){
        $this->cmark->enable_extension('table_of_contents');
        $this->cmark->enable_extension('footnote');
        // var_dump($route->url());
        $rel_path = substr($route->url(), strlen($this->prefix));
        $md_file = $this->dir.'/'.$rel_path;
        if (substr($md_file,-1)=='/')$md_file = substr($md_file,0,-1);
        $md_file .= '.php'; 
            
        ob_start();
        require($md_file);
        // file_get_contents($md_file);
        $response->content = ob_get_contents();

    }

    public function show_files($route, $response){
        // echo 'SHOW FILES!!!!!';
        // exit;
        ob_start();
        // $response->useTheme = false;


        echo '<h1>Files</h1>';

        $files = \Lia\Utility\Files::all($this->dir.'/files/', $this->dir.'/files/');

        $buckets = [];
        foreach ($files as $index=>$path){
            $parts = explode('/',substr($path,1));
            if (count($parts)==1)$bname = '';
            else $bname = array_shift($parts);
            $buckets[$bname][] = implode('/', $parts);
        }
        // var_dump($files);
        // print_r($buckets);
        // exit;
        foreach ($buckets as $name => $files){
            if ($name!='')echo '<h2>'.$name.'</h2>';
            echo '<ul>';
            foreach ($files as $f){
                //this is a list of files nested within directories
                //i also need to list files that are not sorted into sub-directories
                // var_dump($name);
                // exit;
                echo "\n".'<li><a href="'.$this->prefix.'files/'.$this->lia->clean_url($name.'/'.$f).'">'.$f.'</li></a>'."\n";
            }
            echo '</ul>';
        }

        $response->content = ob_get_clean();
    }

    public function toc(){
        return "\n<nav>  \n\n## Table of Contents  \n[TOC]\n\n</nav>\n";
    }

    public function details(string $story_name, string $summary){
        ob_start();
            $this->story($story_name);
        $story = ob_get_clean();
        return "\n\n<details><summary>$summary</summary>\n\n"
            .$story
            ."\n\n</details>\n\n";
            ;
    }

    public function sources(){
        return "\n## Sources  \n".file_get_contents($this->dir.'/sources.md');
    }
}
