## Inline Image example
I was given explicit permission to use this image in my Kitchen Sink app, and hope it is okay for me to use here as well.

<img src="files/right-to-repair.jpg" alt="Right To Repair photo by Jackie Filson / Open Markets Institute" />
<small>Thank you <a href="https://twitter.com/JackieFilson">Jackie Filson</a> and <a href="https://www.openmarketsinstitute.org/">Open Markets Institute</a> For this image.</small>
<small>The Attribution & This text are each inside a &lt;small&gt;</small>
