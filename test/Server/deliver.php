<?php

require(__DIR__.'/../../vendor/autoload.php');

$lia = new \Lia();

$main = \Lia\Package\Server::main($lia);


$cmark = new \Lia\Addon\CommonMark($main);
$story = new \Tlf\Story(__DIR__.'/Stories/sample1/', $lia, '/sample1/');
$story->cmark = $cmark;


// $lia->dump();



$story->setup();

// $stories = new \Tlf\Stories();
// $stories->enable_lia($lia, '/stories/');
//
// $stories->add_dir(__DIR__.'/Stories/');


$lia->deliver();
