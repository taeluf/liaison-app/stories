<?php

namespace Tlf\Stories\Test;

class Main extends \Tlf\Tester {


    public function testRawFile(){
        $content = $this->get('/sample1/files/sample-file.txt');
        $this->compare($content, 'this is a text file');
    }

    public function testFilesPage(){
        $content = $this->get('/sample1/files/');
        echo $content;
        $this->str_contains(
            $content,
            '/sample1/files/right-to-repair.jpg',
            '/sample1/files/sample-file.txt',
        );
    }

    public function testMainPage(){
        $content = $this->get('/sample1/');

        $this->str_contains($content,
            '</a>Title</h1>',
            '<details>',
            '<summary>Terminology</summary>',
            '</a>Background</h2>',
            '<a href="#hh-background">Background</a>',
        );
        echo $content;
    }
}
