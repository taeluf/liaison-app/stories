<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Stories  
Backend software for writing journalistic stories & easily sharing your stories.  
  
This is brand new, beta software & the documentation is poor & implementation details will change.  
  
## Install  
```bash  
composer require taeluf/stories v0.1.x-dev   
```  
or in your `composer.json`  
```json  
{"require":{ "taeluf/stories": "v0.1.x-dev"}}  
```  
  
  
## Getting Started  
You make a `Stories` directory, then in that make a dir for each story you write. Then for each story, you'll need to instantiate a story object ... for now.  
  
Sample deliver script  
```php  
<?php  
  
require(__DIR__.'/../../vendor/autoload.php');  
  
$lia = new \Lia();  
  
$main = \Lia\Package\Server::main($lia);  
  
  
$cmark = new \Lia\Addon\CommonMark($main);  
$story = new \Tlf\Story(__DIR__.'/Stories/sample1/', $lia, '/sample1/');  
$story->cmark = $cmark;  
  
  
// $lia->dump();  
  
  
  
$story->setup();  
  
// $stories = new \Tlf\Stories();  
// $stories->enable_lia($lia, '/stories/');  
//  
// $stories->add_dir(__DIR__.'/Stories/');  
  
  
$lia->deliver();  
```  
  
See the rest of the story files in [test/Server/Stories/sample1/](/test/Server/Stories/sample1/)  
  
`Stories/sample1/story.php`:  
```php  
<?=$this->toc();?>  
  
<?=$this->story('main');?>  
  
<?=$this->story('background');?>  
  
<?=$this->story('inline-image');?>  
  
<?=$this->details('terms', 'Terminology');?>  
  
## File Downloads  
See [File Downloads](files/)  
  
<?=$this->sources()?>  
```  
  
`Stories/sample1/story/main.md`:  
```php  
# Title  
This story isn't about anything at all. It's literally not about anything.  
```  
  
