<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/Story.php  
  
# class Tlf\Story  
  
  
  
## Constants  
  
## Properties  
- `protected string $dir;`   
- `protected string $prefix;`   
- `protected $lia;`   
- `public object $cmark;` an instance of Liaison CommonMark addon  
  
## Methods   
- `public function __construct($dir, $lia, $prefix)`   
- `public function setup()`   
- `public function story($name)` echo the content of a file in your story directory  
- `public function file($name)`   
- `public function sub_stories($name)` show a list of links to sub-stories found at root_story_dir/$name  
- `public function deliver($route, $response)`   
- `public function show_files($route, $response)`   
- `public function toc()`   
- `public function details(string $story_name, string $summary)`   
- `public function sources()`   
  
