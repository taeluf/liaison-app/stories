# Status

## Apr 8, 2022
I've mostly setup this repo & it's probably near working ...

but it was previously setup with lia v0.3 & now lia is v0.5 & the commonmark addon is updated as well & there are some compatibility issues.

I think getting this running will be pretty straightforward. just a bit of `phptest -test Hygienist` and correcting each error as they go. The last error i had was the `instance()` method not existing on the CM liaison addon.
